package br.com.empresa.control;

import java.util.ArrayList;
import java.util.List;

import br.com.empresa.dao.BombaDao;
import br.com.empresa.dao.DaoFactory;
import br.com.empresa.dao.ModeloDao;
import br.com.empresa.dao.VeiculoDao;
import br.com.empresa.entities.Bomba;
import br.com.empresa.entities.Modelo;
import br.com.empresa.entities.Veiculo;
import br.com.empresa.service.JanelaRelatorio;
import br.com.empresa.service.LivroCaixa;

public class PostoCombustivel {

	public static void main(String[] args) {
		DaoFactory daoFactory = DaoFactory.getInstance();
		BombaDao bombaDao = daoFactory.createBombaDao();
		ArrayList<?> bombas = bombaDao.loadAll("bombas.csv", Bomba.class);
		ModeloDao modeloDao = daoFactory.createModeloDao();
		ArrayList<?> modelos = modeloDao.loadAll("modelos.csv", Modelo.class);
		VeiculoDao veiculoDao = daoFactory.createVeiculoDao();
		ArrayList<?> veiculos = veiculoDao.loadAll("veiculos.csv", Veiculo.class);
		for (Object csvRow : veiculos) {
			Veiculo veiculo = (Veiculo) csvRow;
			Bomba bombaEscolhida = veiculo.escolherBomba((List<Bomba>) bombas);
			bombaEscolhida.abastecer(veiculo);
		}
		String relatorio = LivroCaixa.getInstance().imprimir((ArrayList<Bomba>) bombas);
		JanelaRelatorio.getInstance().showInputDialog("Relatório Posto de Gasolina",relatorio);
	}

}
