package br.com.empresa.dao;

import java.util.ArrayList;

import br.com.empresa.entities.Linha;

public interface BombaDao {
	ArrayList<?> loadAll(String fileName, Class clazz);
}
