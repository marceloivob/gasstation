package br.com.empresa.dao;

import java.util.ArrayList;

import br.com.empresa.entities.Linha;

public interface ModeloDao {
	ArrayList<?> loadAll(String fileName, Class clazz);
}
