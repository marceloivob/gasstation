package br.com.empresa.dao;

import br.com.empresa.service.LeitorArquivo;

public class DaoFactory {
	
	private static DaoFactory instance;

	public static DaoFactory getInstance() {
		if (instance == null) {
			instance = new DaoFactory();
		}
		return instance;
	}

	public static ModeloDao createModeloDao() {
		return new ModeloDaoImpl();
	}
	
	public static VeiculoDao createVeiculoDao() {
		return new VeiculoDaoImpl();
	}	
	
	public static BombaDao createBombaDao() {
		return new BombaDaoImp();
	}
}
