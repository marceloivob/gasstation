package br.com.empresa.dao;

import java.util.ArrayList;

import br.com.empresa.entities.Linha;
import br.com.empresa.service.LeitorArquivo;

public class BombaDaoImp implements BombaDao {

	@Override
	public ArrayList<?> loadAll(String fileName, Class clazz) {
		LeitorArquivo fileService = LeitorArquivo.getInstance();
		ArrayList<Linha> bombas = fileService.loadAll(fileName, clazz);
		return bombas;
	}

}
