package br.com.empresa.dao;

import java.util.ArrayList;

import br.com.empresa.entities.Linha;
import br.com.empresa.entities.Veiculo;
import br.com.empresa.service.LeitorArquivo;

public class VeiculoDaoImpl implements VeiculoDao {

	@Override
	public ArrayList<Veiculo> loadAll(String fileName, Class clazz) {
		LeitorArquivo fileService = LeitorArquivo.getInstance();
		ArrayList<Linha> veiculosCsvRow = fileService.loadAll(fileName, clazz);
		ArrayList<Veiculo> veiculos = new ArrayList<>();
		veiculosCsvRow.forEach(v->veiculos.add((Veiculo) v));
		return veiculos;
	}

}
