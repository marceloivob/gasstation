package br.com.empresa.dao;

import java.util.ArrayList;

import br.com.empresa.entities.Linha;
import br.com.empresa.entities.Veiculo;

public interface VeiculoDao {
	ArrayList<Veiculo> loadAll(String fileName, Class clazz);
}
