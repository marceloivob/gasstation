package br.com.empresa.service;

import java.util.Map;

import br.com.empresa.entities.Bomba;
import br.com.empresa.entities.Veiculo;
public class CalculadoraConsumo {
	
	private static CalculadoraConsumo instance;
	
	public static CalculadoraConsumo getInstance() {
		if (instance == null) {
			instance = new CalculadoraConsumo();
		}
		return instance;
	}
	public Double calcular(Veiculo v, Bomba bomba) {
		Map<String, Double> consumosMap = v.getModelo().getConsumosMap();
		return bomba.getPreco()/consumosMap.get(bomba.getTipoCombustivel());
		
	}
}
