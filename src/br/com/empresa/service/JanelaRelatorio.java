package br.com.empresa.service;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
public class JanelaRelatorio extends JFrame {
	private static JanelaRelatorio instance = null;

	public static JanelaRelatorio getInstance() {
		if (instance == null) {
			instance = new JanelaRelatorio();
		}
		return instance;
	}
	public String showInputDialog(String title, String defaultText) {
		JTextArea msg = new JTextArea(defaultText);
		msg.setLineWrap(true);
		msg.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(msg);
		scrollPane.setPreferredSize(new Dimension(600, 320));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		int ris = JOptionPane.showConfirmDialog(null, scrollPane, title, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		if (ris == JOptionPane.OK_OPTION)
			return msg.getText();
		else
			return defaultText;
	}
}