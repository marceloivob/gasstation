package br.com.empresa.service;

import java.util.HashMap;
import java.util.Map;

import br.com.empresa.entities.Linha;

public class MemoriaCache {

	private static MemoriaCache instance = null;

	public static MemoriaCache getInstance() {
		if (instance == null) {
			instance = new MemoriaCache();
		}
		return instance;
	}

	private Map<Class, Map<String, Linha>> map = new HashMap<Class, Map<String, Linha>>();

	public Map<Class, Map<String, Linha>> getMap() {
		return map;
	}

}
