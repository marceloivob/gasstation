package br.com.empresa.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.empresa.entities.Bomba;
import br.com.empresa.entities.Linha;
import br.com.empresa.entities.Modelo;
import br.com.empresa.entities.Veiculo;

public class LeitorArquivo {

	private static LeitorArquivo instance;

	public static LeitorArquivo getInstance() {
		if (instance == null) {
			instance = new LeitorArquivo();
		}
		return instance;
	}

	public ArrayList<Linha> loadAll(String fileName, Class clazz) {
		Map<String, Linha> map = null;
		ArrayList<Linha> valueList = null;
		if (MemoriaCache.getInstance().getMap().containsKey(clazz)) {
			map = MemoriaCache.getInstance().getMap().get(clazz);
		} else {
			try {
				map = new HashMap<String, Linha>();
				valueList = new ArrayList<Linha>();
				BufferedReader reader = new BufferedReader(new FileReader("database/" + fileName));
				String line = null;
				reader.readLine(); // Header
				while ((line = reader.readLine()) != null) {
					Linha c = newCsvRow(clazz, line);
					map.put(c.getNome(), c);
					valueList.add(c);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			MemoriaCache.getInstance().getMap().put(clazz, map);
		}
		return valueList;
	}

	private Linha newCsvRow(Class clazz, String line) {
		String[] split = line.split(Linha.TOKEN_SEPARATOR);
		Linha csvRow = null;
		Map<Class, Map<String, Linha>> map = MemoriaCache.getInstance().getMap();
		Map<String, Linha> clazzMap = map.get(clazz);
		if(clazz.equals(Bomba.class)) {
			String combustivel = split[0];
			String preco = split[1];
			String velocidade = split[2];
			csvRow = new Bomba();
			((Bomba) csvRow).setTipoCombustivel(combustivel);
			((Bomba) csvRow).setPreco(preco);
			((Bomba) csvRow).setVelocidadeAbastecimento(velocidade);
		} else if (clazz.equals(Veiculo.class)) {
			csvRow = new Veiculo();
			String nomeDoModelo = split[0];
			String placa = split[1];
			Modelo modelo = (Modelo) MemoriaCache.getInstance().getMap().get(Modelo.class).get(nomeDoModelo);
			((Veiculo) csvRow).setModelo(modelo);
			((Veiculo) csvRow).setPlaca(placa);
		} else {
			csvRow = new Modelo();
			String modelName = split[0].trim();
			String ec = split[1].replace(",", ".").trim();
			if (!ec.isEmpty()) {
				Double consumoEtanol = Double.valueOf(ec);
				String tipoCombustivel = "Etanol";
				((Modelo) csvRow).registrarConsumo(tipoCombustivel, consumoEtanol);
			}
			String gc = split[2].replace(",", ".").trim();
			if (!gc.isEmpty()) {
				Double consumoGasolina = Double.valueOf(gc);
				String tipoCombustivel = "Gasolina";
				((Modelo) csvRow).registrarConsumo(tipoCombustivel, consumoGasolina);
			}
			Double capacidadeGasolina = Double.valueOf(split[3].trim());
			((Modelo) csvRow).setNome(modelName);
			((Modelo) csvRow).setCapacidadeCombustivel(capacidadeGasolina);
		}
		return csvRow;
	}
}
