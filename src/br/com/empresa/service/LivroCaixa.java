package br.com.empresa.service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import br.com.empresa.entities.Bomba;
import br.com.empresa.entities.Veiculo;
public class LivroCaixa {
	private static LivroCaixa instance = null;

	public static LivroCaixa getInstance() {
		if (instance == null) {
			instance = new LivroCaixa();
		}
		return instance;
	}

	private List<Registrador> listaEventos = new ArrayList<Registrador>();

	public void register(LocalTime instantTime, Bomba bomba, Veiculo v) {
		listaEventos.add(new Registrador(instantTime, v, bomba));
	}

	public String imprimir(ArrayList<Bomba> bombas) {
		StringBuilder sb = new StringBuilder();
		for (Registrador register : listaEventos) {
			sb.append(register.imprimir()+"\n");
		}
		sb.append("\n\nResumo da simulação\n");
		sb.append("-------------------\n");
		for (int i = 0; i < bombas.size(); i++) {
			sb.append("Total abastecido na bomba " + (i + 1) + " (" + bombas.get(i).getTipoCombustivel() + "):"
					+ bombas.get(i).getTotal() + " litros\n");
		}
		return sb.toString();
	}
}

class Registrador {
	public Registrador(LocalTime timeStamp, Veiculo veiculo, Bomba bomba) {
		super();
		this.timeStamp = timeStamp;
		this.veiculo = veiculo;
		this.bomba = bomba;
	}

	private Bomba bomba;
	private LocalTime timeStamp;
	private Veiculo veiculo;
	public String imprimir() {
		return "[" + this.timeStamp + "] Veículo modelo " + veiculo.getModelo().getNome() + ", placa "
				+ veiculo.getPlaca() + " foi abastecido com " + veiculo.getModelo().getCapacidadeCombustivel() + " litros de "
				+ bomba.getTipoCombustivel() + ".";
	}
}
