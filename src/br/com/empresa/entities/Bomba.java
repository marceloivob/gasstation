package br.com.empresa.entities;

import java.time.LocalTime;

import br.com.empresa.service.LivroCaixa;

public class Bomba implements Linha{
	private LocalTime instante = LocalTime.of(0, 0);
	private Double preco;
	private Double velocidadeAbastecimento;
	private String tipoCombustivel;
	private Double total = 0.0;
	private String nome;

	public Bomba() {
	}

	public Double getPreco() {
		return this.preco;
	}

	public Double getVelocidadeAbastecimento() {
		return this.velocidadeAbastecimento;
	}

	public String getTipoCombustivel() {
		return this.tipoCombustivel;
	}

	public void abastecer(Veiculo v) {
		Double capacidade = v.getModelo().getCapacidadeCombustivel();
		double duracao = capacidade / this.velocidadeAbastecimento;
		this.instante = this.instante.plusMinutes(Math.round(duracao));
		LivroCaixa.getInstance().register(instante, this, v);
		this.total += v.getModelo().getCapacidadeCombustivel();
	}

	public Double getTotal() {
		return total;
	}

	public LocalTime getInstante() {
		return instante;
	}

	@Override
	public String getNome() {
		return nome;
	}

	public void setInstante(LocalTime instante) {
		this.instante = instante;
	}

	public void setPreco(String strPreco) {
		Double dblPreco = 0.0;
		if (!strPreco.isEmpty()) {
			String p = strPreco.replace(",", ".").trim();
			dblPreco = Double.valueOf(p);
		}
		this.preco = dblPreco;
	}

	public void setVelocidadeAbastecimento(String strVelocidade) {
		Double dblVelocidade = 0.0;
		if (!strVelocidade.isEmpty()) {
			String v = strVelocidade.replace(",", ".").trim();
			dblVelocidade = Double.valueOf(v);
		}
		this.velocidadeAbastecimento = dblVelocidade;
	}

	public void setTipoCombustivel(String tipoCombustivel) {
		this.tipoCombustivel = tipoCombustivel;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
