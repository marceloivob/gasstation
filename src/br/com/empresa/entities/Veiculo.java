package br.com.empresa.entities;

import java.util.ArrayList;
import java.util.List;

import br.com.empresa.service.CalculadoraConsumo;

public class Veiculo implements Linha {
	private String placa;
	private Modelo modelo;

	public Veiculo() {
	}

	public Bomba escolherBomba(List<Bomba> bombas) {
		Bomba bombaSelecionada = null;
		Double minimun = Double.MAX_VALUE;
		for (int j = 0; j < bombas.size(); j++) {
			String tipoCombustivel = bombas.get(j).getTipoCombustivel();
			boolean existeRegistroConsumo = this.getModelo().getConsumosMap().containsKey(tipoCombustivel);
			if (existeRegistroConsumo) {
				Double costPerKm = CalculadoraConsumo.getInstance().calcular(this, bombas.get(j));
				if (costPerKm < minimun) {
					bombaSelecionada = bombas.get(j);
					minimun = costPerKm;
				}
			}
		}
		return bombaSelecionada;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	@Override
	public String getNome() {
		return this.placa;
	}
	

}
