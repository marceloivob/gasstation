package br.com.empresa.entities;

import java.util.HashMap;
import java.util.Map;

public class Modelo implements Linha {
	private String nome;
	private Map<String, Double> consumosMap = new HashMap<String, Double>();
	private Double capacidadeCombustivel;

	public Modelo() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setConsumosMap(Map<String, Double> consumosMap) {
		this.consumosMap = consumosMap;
	}

	public Double getCapacidadeCombustivel() {
		return capacidadeCombustivel;
	}

	public void setCapacidadeCombustivel(Double capacidadeCombustivel) {
		this.capacidadeCombustivel = capacidadeCombustivel;
	}

	public void registrarConsumo(String tipoCombustivel, Double consumo) {
		this.consumosMap.put(tipoCombustivel, consumo);
	}

	public Map<String, Double> getConsumosMap() {
		return consumosMap;
	}

}
